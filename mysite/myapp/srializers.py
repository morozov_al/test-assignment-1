from rest_framework import serializers
from .models import Wallet, Transaction


class WalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = (
            'id',
            'assigned',
            'balance_rub',
            'balance_usd'
        )


class AllTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = (
            'id',
            'sender',
            'recipient',
            'created_at',
            'currency_type',
            'amount',
        )


class TransactionSerializer(serializers.ModelSerializer):
    currency_type = serializers.CharField(required=True)
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)
    sender = serializers.IntegerField(required=True)
    recipient = serializers.IntegerField(required=False)

    class Meta:
        model = Transaction
        fields = (
            'sender',
            'recipient',
            'created_at',
            'currency_type',
            'amount',
        )

    def validate(self, data):
        sender = data.get('sender')
        recipient = data.get('recipient')

        if sender == recipient:
            raise serializers.ValidationError("Sender and recipient cannot be the same.")

        if data.get("currency_type") not in dict(Transaction.CURRENCY_TYPES):
            raise serializers.ValidationError("Wrong currency type.")

        if not Wallet.objects.filter(id=data.get("sender")).exists():
            raise serializers.ValidationError("Error.")

        if not Wallet.objects.filter(id=data.get("recipient")).exists():
            raise serializers.ValidationError("This wallet does not exist.")

        return data
