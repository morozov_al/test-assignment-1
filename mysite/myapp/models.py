from django.db import models
from django.contrib.auth.models import User


class Wallet(models.Model):
    """
    Model Wallet ordered by "id" field
    """

    assigned = models.ForeignKey(User, on_delete=models.CASCADE)
    balance_rub = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    balance_usd = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

    class Meta:
        ordering = ['id']


class Transaction(models.Model):
    """
    Model Transaction ordered by "created_at" field
    """

    CURRENCY_TYPES = [
        ('rub', 'Rubles'),
        ('usd', 'US Dollar')
    ]

    sender = models.ForeignKey(
        Wallet, on_delete=models.SET_NULL, null=True, blank=True, related_name='sent_transactions'
    )
    recipient = models.ForeignKey(
        Wallet, on_delete=models.SET_NULL, null=True, blank=True, related_name='received_transactions'
    )
    currency_type = models.CharField(max_length=10, choices=CURRENCY_TYPES)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)


    class Meta:
        ordering = ['created_at']

