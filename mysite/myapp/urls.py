from django.urls import path, include
from .views import (
    TransactionCreateView,
    WalletViewSet,
    WalletTransactionsAPIView,
)
from rest_framework.routers import DefaultRouter

app_name = "myapp"

router = DefaultRouter()
router.register(r'wallets', WalletViewSet, basename='wallet')


urlpatterns = [
    path("", include(router.urls), name="wallets"),
    path("transaction/create/", TransactionCreateView.as_view(), name="transactions"),
    path('transaction/<int:wallet_id>/', WalletTransactionsAPIView.as_view(),
         name='wallet_transactions_api'),

]