from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Wallet, Transaction

class WalletInline(admin.TabularInline):
    model = Wallet
    extra = 1

class TransactionSentInline(admin.TabularInline):
    model = Transaction
    fk_name = 'sender'
    extra = 1

class TransactionReceivedInline(admin.TabularInline):
    model = Transaction
    fk_name = 'recipient'
    extra = 1

class WalletAdmin(admin.ModelAdmin):
    model = Wallet
    inlines = [TransactionSentInline, TransactionReceivedInline]
    list_display = ('id', 'assigned', 'balance_rub', 'balance_usd')


class UserAdmin(BaseUserAdmin):
    inlines = [WalletInline]

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Wallet, WalletAdmin)
admin.site.register(Transaction)