from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.viewsets import ModelViewSet
from .srializers import (
    WalletSerializer,
    TransactionSerializer,
    AllTransactionSerializer,
)
from .models import Wallet, Transaction
from django.db import transaction
from decimal import Decimal, ROUND_HALF_UP


class WalletViewSet(ModelViewSet):
    """
    API view for CRUD on Wallet object
    Show all wallets assigned with user

    """

    serializer_class = WalletSerializer
    queryset = Wallet.objects.all()

    def get_queryset(self):
        return Wallet.objects.filter(assigned=self.request.user)


class WalletTransactionsAPIView(APIView):
    def get(self, request, wallet_id, format=None):
        wallet = get_object_or_404(Wallet, id=wallet_id)
        transactions = Transaction.objects.filter(
            sender=wallet
        ) | Transaction.objects.filter(recipient=wallet)
        serializer = AllTransactionSerializer(transactions, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class TransactionCreateView(APIView):
    """
    API view for create transactions
    """

    serializer_class = TransactionSerializer

    @transaction.atomic
    def post(self, request: Request):
        serializer = TransactionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = request.data
        sender = Wallet.objects.get(id=data.get("sender"))
        if request.user != sender.assigned:
            return Response(
                {"transaction_error": "It is not ypur wallet"},
                status=status.HTTP_404_NOT_FOUND,
            )
        amount = Decimal(data.get("amount")).quantize(
            Decimal("0.00"), rounding=ROUND_HALF_UP
        )
        if data.get("currency_type") == "rub":
            sender_balance = Wallet.objects.get(id=data.get("sender"))
            if sender_balance.balance_rub >= amount:
                recipient_balance = Wallet.objects.get(id=data.get("recipient"))
                sender_balance.balance_rub -= amount
                recipient_balance.balance_rub += amount
                sender_balance.save()
                recipient_balance.save()
                Transaction.objects.create(
                    sender=sender,
                    recipient=recipient_balance,
                    currency_type=data.get("currency_type"),
                    amount=amount,
                )

                return Response(
                    data={
                        "assigned": sender_balance.assigned.id,
                        "balance_rub": sender_balance.balance_rub,
                        "balance_usd": sender_balance.balance_usd,
                    },
                    status=status.HTTP_200_OK,
                )

            return Response(
                {"transaction_error": "Not enough fund"},
                status=status.HTTP_404_NOT_FOUND,
            )

        if data.get("currency_type") == "usd":
            print(data.get("sender"))
            sender_balance = Wallet.objects.get(id=data.get("sender"))
            if sender_balance.balance_usd >= amount:
                recipient_balance = Wallet.objects.get(id=data.get("recipient"))
                sender_balance.balance_usd -= amount
                recipient_balance.balance_usd += amount
                sender_balance.save()
                recipient_balance.save()
                sender = Wallet.objects.get(id=data.get("sender"))
                Transaction.objects.create(
                    sender=sender,
                    recipient=recipient_balance,
                    currency_type=data.get("currency_type"),
                    amount=amount,
                )

                return Response(
                    data={
                        "assigned": sender_balance.assigned.id,
                        "balance_rub": sender_balance.balance_rub,
                        "balance_usd": sender_balance.balance_usd,
                    },
                    status=status.HTTP_200_OK,
                )

            return Response(
                {"transaction_error": "Not enough fund"},
                status=status.HTTP_404_NOT_FOUND,
            )

        return Response({"siso": 123})
