from django.urls import path
from django.contrib.auth.views import LoginView
from .views import Logout, Registration, AboutMe

app_name = 'myauth'

urlpatterns = [
    path(
        "login/",
        LoginView.as_view(
            template_name="myauth/login.html", redirect_authenticated_user=True
        ),
        name="login",
    ),
    path("logout/", Logout.as_view(), name="logout"),
    path("registration/", Registration.as_view(), name="register"),
    path("about-me/", AboutMe.as_view(), name="about-me"),

]