from django.contrib.auth import authenticate, login
from django.contrib.auth.views import LogoutView, TemplateView
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views.generic import CreateView


class Logout(LogoutView):
    """
    Logout view
    """

    next_page = reverse_lazy("myauth:login")


class Registration(CreateView):
    """
    View for creating new user
    """

    form_class = UserCreationForm
    template_name = "myauth/register.html"
    success_url = reverse_lazy("myauth:about-me")

    def form_valid(self, form):
        response = super().form_valid(form)
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password)
        login(request=self.request, user=user)
        return response


class AboutMe(TemplateView):
    """
    View for getting info about logged-in user
    """

    template_name = "myauth/about-me.html"
